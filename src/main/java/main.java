import javax.swing.*;

public class main {
    private static final String INVALID_VALUE_MESSAGE = "Invalid value";
    private static final String SPCER = "===========================================================================";











    public static void printNumberInWord (int number){
        switch (number){
            case 0:
                System.out.println("ZERO");
                break;
            case 1:
                System.out.println("ONE");
                break;
            case 2:
                System.out.println("TWO");
                break;
            case 3:
                System.out.println("THREE");
                break;
            case 4:
                System.out.println("FOUR");
                break;
            case 5:
                System.out.println("FIVE");
                break;
            case 6:
                System.out.println("SIX");
                break;
            case 7:
                System.out.println("SEVEN");
                break;
            case 8:
                System.out.println("EIGHT");
                break;
            case 9:
                System.out.println("NINE");
                break;
            default:
                System.out.println("OTHER");
        }

    }



    public static void printDayOfTheWeek (int day){

        switch (day){
            case 0:
                System.out.println("Sunday");
                break;
            case 1:
                System.out.println("Monday");
                break;
            case 2:
                System.out.println("Tuesday");
                break;
            case 3:
                System.out.println("Wednesday");
                break;
            case 4:
                System.out.println("Thursday");
                break;
            case 5:
                System.out.println("Friday");
                break;
            case 6:
                System.out.println("Saturday");
                break;
            default:
                System.out.println("Invalid day");
        }

    }


    public static boolean isCatPlaying(boolean summer,int temperature){
        if(summer){
            if(temperature>=25 && temperature<= 45){
                return true;
            }else return false;
        }else {
            if (temperature>=25 && temperature<=35){
                return true;
            }else return false;
        }

    }


    public static void printEqual(int number1, int number2, int number3) {
        if (number1 < 0 || number2 < 0 || number3 < 0) {
            System.out.println("Invalid Value");
        } else if (number1 == number2 && number2 == number3) {
            System.out.println("All numbers are equal");
        } else if (number1 != number2 && number2 != number3 && number1 != number3) {
            System.out.println("All numbers are different");
        } else {
            System.out.println("Neither all are equal or different");
        }


    }


    public static void printYearsAndDays(long minutes) {
        if (minutes < 0) {
            System.out.println("Invalid Value");
        } else {

            long year = minutes / 525600;
            long day = (minutes / 1440) - (year * 365);

            System.out.println(minutes + " min = " + year + " y and " + day + " d");
        }

    }

    public static double area(double radius) {
        double circleCalculatedArea = 0;
        if (radius < 0) {
            return -1.0;
        }
        circleCalculatedArea = Math.PI * Math.pow(radius, 2);
        return circleCalculatedArea;

    }

    public static double area(double x, double y) {
        double calculatedArea = 0;
        if (x < 0 || y < 0) {
            return -1.0;
        }
        calculatedArea = x * y;
        return calculatedArea;
    }



    private static String getDurationString(long minutes, long seconds) {
        if ((minutes < 0) || (seconds < 0) || (seconds > 59)) {
            return INVALID_VALUE_MESSAGE;
        }
        long hours = minutes / 60;
        long remainingMinutes = minutes % 60;
        String hourString = hours + "h ";
        if (hours < 10) {
            hourString = "0" + hourString;
        }
        String minutesString = remainingMinutes + "m ";

        if (remainingMinutes < 10) {
            minutesString = "0" + minutesString;
        }
        String secondsString = seconds + "s";
        if (seconds < 10) {
            secondsString = "0" + secondsString;
        }
        return hourString + minutesString + secondsString;

    }

    private static String getDurationString(long seconds) {
        if ((seconds < 0)) {
            return INVALID_VALUE_MESSAGE;

        }
        long minutes = seconds / 60;
        long remainingSeconds = seconds % 60;
        return getDurationString(minutes, remainingSeconds);


    }


    public static double calcFeetAndInchesToCentimeters(double feet, double inches) {
        double rezultat = 0;

        if (feet < 0) {
            rezultat = -1;
        }
        if (inches < 0 || inches > 12) {
            rezultat = -1;
        } else {
            rezultat = feet * 12 * 2.54 + inches * 2.54;

        }
        return rezultat;


    }

    public static double calcFeetAndInchesToCentimeters(double inches) {

        if (inches < 0) {
            return -1;
        }
        double feets = (int) inches / 12;
        double remainingInches = (int) inches % 12;


        return calcFeetAndInchesToCentimeters(feets, remainingInches);
    }


    public static boolean hasTeen(int person1, int person2, int person3) {
        boolean rezultat = false;
        if (person1 >= 13 & person1 <= 19) {
            rezultat = true;
        } else if (person2 >= 13 & person2 <= 19) {
            rezultat = true;
        } else if (person3 >= 13 & person3 <= 19) {
            rezultat = true;
        }
        return rezultat;

    }

    public static boolean isTeen(int person) {
        return person >= 13 & person <= 19;
    }


    public static boolean hasEqualSum(int num1, int num2, int sumCkd) {
        return (num1 + num2) == sumCkd;

    }

    public static boolean areEqualByThreeDecimalPlaces(double firstNumber, double secondNumber) {
        return (int) (Math.pow(10, 3) * firstNumber) == (int) (Math.pow(10, 3) * secondNumber);

    }

    public static long toMilesPerHour(double kilometersPerHour) {
        if (kilometersPerHour < 0) {
            return -1;

        }
        return Math.round(kilometersPerHour / 1.609);

    }

    public static void printConversion(double kilometersPerHour) {

        if (kilometersPerHour < 0) {
            System.out.println("Invalid value");

        } else {
            long milesPerHour = toMilesPerHour(kilometersPerHour);
            System.out.println(kilometersPerHour + " km/h= " + milesPerHour + " mi/h");
        }
    }


    public static void printMegaBytesAndKiloBytes(int kiloBytes) {
        if (kiloBytes < 0) {
            System.out.println("Invalid Value");
        } else {
            int megabytes = kiloBytes / 1024;
            int restOfkiloBytes = kiloBytes % 1024;
            System.out.println(kiloBytes + " KB = " + megabytes + " MB and " + restOfkiloBytes + " KB");

        }
    }

    public static boolean shouldWakeUp(boolean barking, int hourOfDay) {
        if (barking) {
            if (hourOfDay < 0 || hourOfDay > 23) {
                return false;
            } else if (hourOfDay < 8 || hourOfDay > 22) {
                return true;
            } else if (hourOfDay == 8 || hourOfDay == 22) {
                return false;
            }
        } else {
            System.out.println("dog is not barking.");
        }
        return false;
    }

    public static boolean isLeapYear(int year) {
        boolean rezultat = false;
        if (year < 1 || year > 9999) {
            rezultat = false;
        } else if (year % 400 == 0) {
            rezultat = true;
        } else if (year % 100 == 0) {
            rezultat = false;
        } else if (year % 4 == 0) {
            rezultat = true;
        }
        return rezultat;
    }


    public static int calculateScore(boolean gameOver, int score, int levelCompleted, int bonus) {
        if (gameOver) {
            int finalScore = score + (levelCompleted * bonus);
            finalScore += 11;
            return finalScore;
        } else {
            return -1;
        }
    }

    public static void displayHighScorePosition(String playerName, int playerPosition) {
        System.out.println(playerName + " managed to get into position " + playerPosition + " on the hight score table");


    }

    public static int calculateHighScorePosition(int playerScore) {
        int playerPosition = 0;
        if (playerScore >= 1000) {
            playerPosition = 1;
        } else if (playerScore >= 500) {
            playerPosition = 2;
        } else if (playerScore >= 100) {
            playerPosition = 3;
        } else if (playerScore < 100) {
            playerPosition = 4;
        }
        return playerPosition;


    }


}



