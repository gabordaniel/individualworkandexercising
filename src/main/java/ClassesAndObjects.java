
public class ClassesAndObjects {
    public static void main(String[] args) {



        Person John = new Person("Jon");
        John.setAge(23);

        Person Alex = new Person("Alexandru");
        Alex.setAge(18);

        John.sayHelloTo(Alex);


        System.out.println(John.getName() + " is " + John.getAge() + " years old");
        System.out.println(Alex.getName() + " is " + Alex.getAge() + " years old");

    }
}
